package fulljava;

import java.util.Scanner;

public class UserInput {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter first number :- ");
		int num1 = scan.nextInt(); 
		System.out.println();
		
		System.out.println("Please enter secound number :- ");
		int num2 = scan.nextInt(); 
		System.out.println();

		System.out.println(num1 + " + " + num2 + " = " + (num1+num2));
		System.out.println(num1 + " - " + num2 + " = " + (num1-num2));
		System.out.println(num1 + " * " + num2 + " = " + (num1*num2));
		System.out.println(num1 + " / " + num2 + " = " + (num1/num2));
		System.out.println(num1 + " % " + num2 + " = " + (num1%num2));

	}

}
