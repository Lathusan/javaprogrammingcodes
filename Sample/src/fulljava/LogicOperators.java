package fulljava;

public class LogicOperators {
	
	public static void main(String[] args) {
		
		boolean A1 = true;
		boolean B2 = false;
	
		System.out.println( "A1 : " + A1 );
		System.out.println( "B2 : " + B2 );
		System.out.println( "A1 and B2 : " + (A1 && B2) );
		System.out.println( "A1 or B2 : " + (A1 || B2) );
		System.out.println( "!A1 : " +  !A1 );
		
	}

}
