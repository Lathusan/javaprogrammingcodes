package fulljava;

public class AritmeticOperators {
	
	public static void main(String[] args) {
		
		int i=100;
		int j=30;
		
		System.out.println(i + " + " + j + " = " + (i + j));
		System.out.println(i + " - " + j + " = " + (i - j));
		System.out.println(i + " * " + j + " = " + (i * j));
		System.out.println(i + " / " + j + " = " + (i / j));
		System.out.println(i + " % " + j + " = " + (i % j));
		
	}

}
