package fulljava;

public class SwitchBreak {

	public static void main(String[] args) {
		
		int num = 1;
		
		switch (num) {
		
			case 1 : 
			
				System.out.println("1  " + num);
				
			break;
				
			case 2 : 
			
				System.out.println("2  " + num);
			
			break;
				
			default :
		
				System.out.println("d  " + num);
			
		}
		
	}
	
}
