package fulljava;

public class SwitchStatement {

	public static void main(String[] args) {
		
		int num = 1;
		
		switch (num) {
		
			case 1 : 
			
				System.out.println(num);
				
			case 2 : 
			
				System.out.println(num);
			
			default :
		
				System.out.println(num);
			
		}
		
	}
	
}
